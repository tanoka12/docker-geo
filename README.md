## Preparar docker para ElasticSearch

- En Windows/Mac con Docker-Toolbox ejecutar en consola

````
docker-machine ssh
sudo sysctl -w vm.max_map_count=262144
````

- Mas Info en https://www.elastic.co/guide/en/elasticsearch/reference/current/docker.html


## Download project 

```
git clone https://gitlab.com/tanoka12/docker-geo.git
```

## Levantar los contenedores

```
docker-compose up 
```

## Verificar los contenedores

- Las url por defecto serán **localhost** a no ser que usemos docker-machine que sera la ip privada que te asigne docker-machine al levantar la máquina virtual

- Abrimos un navegador

http://localhost:9200/

```json
{
  "name" : "hot",
  "cluster_name" : "cluster-geo",
  "cluster_uuid" : "AAoIAV-ZQkmovl-86hLEww",
  "version" : {
    "number" : "6.0.0",
    "build_hash" : "8f0685b",
    "build_date" : "2017-11-10T18:41:22.859Z",
    "build_snapshot" : false,
    "lucene_version" : "7.0.1",
    "minimum_wire_compatibility_version" : "5.6.0",
    "minimum_index_compatibility_version" : "5.0.0"
  },
  "tagline" : "You Know, for Search"
}
```

- Verificar kibana

````
http://localhost:5601
````

![kibana](img/kibana.png)

## Parar contenedores

```
docker-compose stop 
```

## Eliminar contenedore
```
docker-compose rm 
```



